$('.slider-main').slick({
    slidesToShow: 1,
    arrows: false,
    asNavFor: '.slider-nav',
    vertical: true,
    autoplay: true,
    verticalSwiping: true,
    centerMode: true
});

$('.slider-nav').slick({
    slidesToShow: 4,
    asNavFor: '.slider-main',
    vertical: true,
    focusOnSelect: true,
    autoplay: false,
    centerMode: true
});

$(window).on('resize orientationchange', function() {
    if ($(window).width() > 1200) {
        $('.slider-nav').slick('unslick');
        $('.slider-nav').slick({
            slidesToShow: 4,
            asNavFor: '.slider-main',
            vertical: true,
            focusOnSelect: true,
            autoplay: false,
            centerMode: true
        });
    }
});


var fullHeight = function() {

    if ( !isMobile.any() ) {
        $('.js-fullheight').css('height', $(window).height());
        $(window).resize(function(){
            $('.js-fullheight').css('height', $(window).height());
        });
    }

};